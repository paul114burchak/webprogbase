let model = {
    cars: [],
    filterString: "",

    get filteredCars() {
        let filter = model.filterString.toLowerCase().trim();
        return !filter ? this.cars : this.cars.filter(x => {
            return x.model.toLowerCase().includes(filter);
        });
    }
};

window.addEventListener('load', function() {
    let inputEl = document.getElementById('search');
    inputEl.addEventListener('input', function(e) {
        console.log(e.target.value);
        model.filterString = e.target.value;

        renderCars(1);
    });

    let pageBtns = document.getElementsByClassName('pageButton');
    console.log(pageBtns);
    for (let i =0; i < pageBtns.length; i++)
    pageBtns[i].addEventListener('click', function(e) {
        console.log(e.target.innerText);

        renderCars(parseInt(e.target.innerText));
    });

    inputEl.value = model.filterString;
    //let page = document.getElementById("page").innerText;
    requestCars();    
});

function requestCars() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    console.log(xhttp.readyState, xhttp.response, xhttp.status);
      if (this.readyState == 4 && this.status == 200) {
        let cars = JSON.parse(xhttp.response);
        model.cars = cars;
        console.log(cars);
        renderCars(1);
      }
    };
    xhttp.open("GET", "/api/v1/cars", true);
    xhttp.send();
}

function renderCars(page) {
    let carsEl = document.getElementById('cars');
    carsEl.innerHTML = "";
    let filteredCars = [];


    // let paginationEl = document.getElementById('pagination');
    // var templateagination = document.getElementById("paginationScript").innerHTML;
    // let pages = [];
    // for(let i = 1; i <= Math.ceil(model.filteredCars.length/4);i++) {
    //     pages.push({i});
    // }
    // console.log(pages);
    // var renderedHTMLPagination = Mustache.render(templateagination, { pages: pages });
    // paginationEl.innerHTML = renderedHTMLPagination;

    for (let i = (page - 1) * 4; i < ((model.filteredCars.length / 4 > page) ? (page - 1) * 4 + 4 : model.filteredCars.length); i++) {
        console.log(model.filteredCars[i],i);
        filteredCars.push(model.filteredCars[i]);
    }
    console.log("zdarova");
    console.log(filteredCars);
    var template = document.getElementById("car-container").innerHTML;
    var renderedHTML = Mustache.render(template, { cars: filteredCars });
    carsEl.innerHTML = renderedHTML;
    //for (let car of filteredCars) {
        // let aEl = document.createElement('a');
        // aEl.setAttribute('href', '/auto/' + car.id);
        // let scaleEl = document.createElement('div');
        // scaleEl.className = "scale";
        // scaleEl.setAttribute('style', 'background: url(/image/'+ car.pic2 +') no-repeat left top;\
        // background-size: 100% 100%;');
        // let spanEl = document.createElement('span');
        // spanEl.className = "mylabel";
        // spanEl.innerText = car.model;

        // scaleEl.appendChild(spanEl);
        // aEl.appendChild(scaleEl);
        // carsEl.appendChild(aEl);
        var template = document.getElementById("car-container").innerHTML;
        // згенерувати HTML строку на основі шаблону і даних
        var renderedHTML = Mustache.render(template, { cars: filteredCars });
        // помістити строку з HTML всередину елемента з ідентифікатором "my-list"
        carsEl.innerHTML = renderedHTML;
    
    //}
}