const mongoose = require('mongoose');
let  mongoosePaginate = require('mongoose-paginate');

const url = "mongodb://localhost:27017/db";

mongoose.connect(url);
let Schema = mongoose.Schema;

let schema = new Schema({
    _id: Number,
    model: String,
    size_class: String,
    doors: Number,
    engine_volume: Number,
    engine_type: String,
    start_date: String,
    pic1: String,
    pic2: String
});
//schema.plugin(mongoosePaginate);

let Image = mongoose.model('Image', {
    data: Buffer,
    imageName: String,
    contentType: String
})

// function paginateCars(page) {
//     return car.paginate({}, {page: page, limit:4})
//     .then (doc=> {return Promise.resolve(doc)})
//     .catch (err => {return Promise.reject(err)});
// }

function updloadImage(data1, imgName1, type1, data2, imgName2, type2) {
    let Image1 = new Image({
        data: data1,
        imageName: imgName1,
        contentType: type1
    })
    let Image2 = new Image({
        data: data2,
        imageName: imgName2,
        contentType: type2
    })
    const promise = Image1.save()
        .then(() => Image2.save())
        .catch(err => { throw err })
    return promise;
}

function updateImage(x) {
    let updates = new Image(x);
    const promise = Image.findOneAndUpdate(
        { imageName: x.imageName },
        { $set: updates })
        .then(img => { return img })
        .catch(error => { throw error + x_id; });
    return promise;
};

function getImage(imagename) {
    let promise = Image.findOne({ imageName: imagename })
        .then(img => {
            return img;
        })
        .catch(error => { throw error + imagename; });
    return promise;
}

let car = mongoose.model('car', schema);

function create(x) {
    let newCar = new car(x);
    const promise = newCar.save()
        .then(y => console.log("Created: %s", y))
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
};

function getAll() {
    const promise = car.find()
        .then(cars => {
            return cars;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
};

function getById(x_id) {
    const promise = car.findOne({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function update(x) {
    let updates = new car(x);
    const promise = car.findOneAndUpdate(
        { _id: x._id },
        { $set: updates })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function remove(x_id) {
    const promise = car.remove({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
}

let SchemaUser = new Schema({
    _id: Number,
    username: String,
    password: String,
    role: String,
});

let user = mongoose.model('user', SchemaUser);

function userCreate(x) {
    let newUser = new user(x);
    const promise = newUser.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function userGetById(x_id) {
    const promise = user.findOne({ _id: x_id })
        .then(user => { return user; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function userGetByUsername(x_username) {
    const promise = user.findOne({ username: x_username })
        .then(user => {
            console.log('user received by x_username', user);
            if (user === null) throw 'not found';
            return user;
        })
    //.catch(error => { throw error + x_username; });
    return promise;
}

function userGet(x_username, x_password) {
    const promise = user.findOne({ username: x_username, password: x_password })
        .then(user => {
            console.log("user by password");
            if (user === null) throw 'not found';
            return user;
        });
    return promise;
}

function usersGetAll() {
    const promise = user.find()
        .then(users => {
            return users;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
}



module.exports = {
    getAll,
    getById,
    update,
    remove,
    create,
    userCreate,
    userGet,
    userGetById,
    usersGetAll,
    userGetByUsername,
    updloadImage,
    getImage,
    updateImage
};