const storage = require('../modules/storage.js');
const auth = require('../modules/auth.js');
const express = require('express');
const bodyParser = require('body-parser');
let router = express.Router();
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());
router.get('/',
    (req, res) => {
        let message = "";
        res.render('register', { user: req.user, message });
    });


const serverSalt = "45%sAlT_";
router.post('/',
    (req, res) => {
        let username = req.body.username;
        let pass = req.body.pass;
        let pass2 = req.body.pass2;
        let role = 'user';
        // @todo перевірити валідність даних і створити нового користувача у БД 
        if (pass === pass2) {
            storage.userCreate({
                "_id": Math.round(Math.random() * (10000 - 1000) + 1000),
                "username": username,
                "password": sha512(pass, serverSalt).passwordHash,
                "role": role
            })
                .then(user => res.redirect('/'))
                .catch(error => {
                    console.log(error);
                    res.sendStatus(404);
                });
        } else {
            const message = "Passwords don't match";
            res.render('register', { user: req.user, message });
        }
    });

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};
module.exports = router;