const storage = require('../modules/storage.js');
const auth = require('../modules/auth.js');
const express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

router.get('/:auto_id(\\d+)',
    auth.checkAuth,
    (req, res, next) => {
        storage.getById(parseInt(req.params.auto_id))
            .then(auto => {
                res.render('auto', {
                    auto, user: req.user
                });
            })
            .catch(err => {
                console.log(err);
                res.sendStatus(404);
            });
    });

router.post('/delete/:auto_id(\\d+)',
    auth.checkAdmin,
    (req, res) => {
        const id = parseInt(req.params.auto_id);
        storage.remove(id);
        res.redirect('/autos?page=1');
    });

module.exports = router;
