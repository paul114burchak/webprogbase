const storage = require('../modules/storage.js');
const auth = require('../modules/auth.js');
const express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

router.get('/',
    auth.checkAuth,
    (req, res) => {
        const page = parseInt(req.query.page);
        storage.getAll()
            .then(cars => {
                res.render('autos', {
                    cars, page, user: req.user
                });
            })
            .catch(err => {
                console.log(err);
                res.sendStatus(404);
            });

    });

router.get('/search/',
    auth.checkAuth,
    (req, res) => {
        const page = parseInt(req.query.page);
        const search = req.query.search;
        storage.getAll()
            .then(cars => {
                let carsFilter = [];
                for (let i in cars)
                    if (cars[i].model.includes(search))
                        carsFilter.push(cars[i]);

                cars = carsFilter.slice();
                res.render('autos', {
                    cars, page, search, user: req.user
                });
            })
            .catch(err => {
                console.log(err);
                res.sendStatus(404);
            });
    });

module.exports = router;
