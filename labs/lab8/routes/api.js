const express = require('express');
const bodyParser = require('body-parser');
const storage = require('../modules/storage.js');
const check = require('../modules/auth.js');
const fileload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
let path = require('path');
let auth = require('basic-auth');
let router = express.Router();

router.use(express.static('public'));
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(fileload());
router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function basicAuth(callback, param, req, res, role) {
    let credentials = auth(req);
    if (credentials) {
        storage.userGet(credentials.name, sha512(credentials.pass, serverSalt).passwordHash)
            .then(user => {
                if (role) {
                    if (user.role !== role && param !== user.username) {
                        console.log("hello");
                        throw "Forbiden";
                    }
                }
                callback(param);
            })
            .catch(error => {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ "message": error, "documentation_url": "docs/api/v1" }, null, 2));
            });
    } else {
        res.set('WWW-Authenticate', 'Basic realm="example"');
        //res.send(JSON.stringify({ "message": "Required auth", "documentation_url": "docs/api/v1" }, null, 2));
        res.send(401);
    }
}

router.get('/api/v1/users',
    (req, res) => {
        function getAllUsers() {
            storage.usersGetAll()
                .then(users => {
                    let outUsers = [];
                    for (let user of users)
                        outUsers.push({
                            "id": user._id,
                            "username": user.username,
                            "role": user.role,
                            "api_url": "/api/v1/user/" + user.username
                        });

                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outUsers, null, 2));
                })
        }
        basicAuth(getAllUsers, null, req, res, "admin");
    });

router.get('/api/v1/users/:page',
    (req, res) => {
        function getAllUsers() {
            let page = req.params.page;
            storage.usersGetAll()
                .then(users => {
                    if (page == 0 || page * 4 >= users.length + 4) throw "Wrong index";
                    let outUsers = [];
                    for (let i = (page - 1) * 4; i < ((users.length / 4 > page) ? (page - 1) * 4 + 4 : users.length); i++) {
                        outUsers.push({
                            "id": users[i]._id,
                            "username": users[i].username,
                            "role": users[i].role,
                            "api_url": "/api/v1/user/" + users[i].username
                        });
                    }
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outUsers, null, 2));
                })
                .catch(error => {
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": error, "documentation_url": "docs/api/v1" }, null, 2));
                });
        }
        basicAuth(getAllUsers, null, req, res, "admin");
    });

router.get('/api/v1/cars',
    (req, res) => {
        function getAllCars() {
            storage.getAll()
                .then(cars => {
                    let outCars = [];
                    for (let car of cars)
                        outCars.push({
                            "id": car._id,
                            "model": car.model,
                            "size_class": car.size_class,
                            "doors": car.doors,
                            "engine_volume": car.engine_volume,
                            "engine_type": car.engine_type,
                            "start_date": car.start_date,
                            "pic1": car.pic1,
                            "pic2": car.pic2,
                            "api_url": "/api/v1/car/" + car._id
                        });

                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outCars, null, 2));
                });
        }
        basicAuth(getAllCars, null, req, res);
    });


router.get('/api/v1/cars/:page',
    (req, res) => {
        function getAllCars() {
            let page = req.params.page;
            storage.getAll()
                .then(cars => {
                    if (page === 0 || page * 4 >= cars.length + 4) throw "Wrong index";
                    let outCars = [];
                    for (let i = (page - 1) * 4; i < ((cars.length / 4 > page) ? (page - 1) * 4 + 4 : cars.length); i++) {
                        outCars.push({
                            "id": cars[i]._id,
                            "model": cars[i].model,
                            "size_class": cars[i].size_class,
                            "doors": cars[i].doors,
                            "engine_volume": cars[i].engine_volume,
                            "engine_type": cars[i].engine_type,
                            "start_date": cars[i].start_date,
                            "pic1": cars[i].pic1,
                            "pic2": cars[i].pic2,
                            "api_url": "/api/v1/car/" + cars[i]._id
                        });
                    }
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outCars, null, 2));
                })
                .catch(error => {
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": error, "documentation_url": "docs/api/v1" }, null, 2));
                });
        }
        basicAuth(getAllCars, null, req, res);
    });

router.delete('/api/v1/cars/delete/:id(\\d+)',
    (req, res) => {
        function deleteCar() {
            const id = parseInt(req.params.id);
            storage.remove(id).
                then(() => {
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": "Object deleted", "documentation_url": "docs/api/v1" }, null, 2));
                });
        }
        basicAuth(deleteCar, null, req, res);
    });

router.post('/api/v1/cars/add',
    (req, res) => {
        function addCar() {
            if (req.files.pic1 != undefined || req.files.pic2 != undefined) {
                let fname1 = req.files.pic1.name;
                let fname2 = req.files.pic2.name;
                storage.create({
                    "_id": Math.round(Math.random() * (10000 - 1000) + 1000),
                    "model": req.body.model,
                    "size_class": req.body.size_class,
                    "doors": parseInt(req.body.doors),
                    "engine_volume": parseInt(req.body.engine_volume),
                    "engine_type": req.body.engine_type,
                    "start_date": req.body.start_date,
                    "pic1": fname1,
                    "pic2": fname2
                })
                    .then(() => {
                        if (fname1.match(/\.(jpeg|jpg|gif|png)$/) != null && fname2.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                            let format1 = fname1.split('.').pop();
                            let format2 = fname2.split('.').pop();
                            storage.updloadImage(req.files.pic1.data, fname1, format1, req.files.pic2.data, fname2, format2)
                                .catch(err => { console.error(err) })
                        } else {
                            console.error("Only images are allowed!");
                        }
                    })
                    .then(() => {
                        console.log("after img");
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({ "message": "Object created", "documentation_url": "docs/api/v1" }, null, 2));
                    })
                    .catch(error => {
                        console.log(error);
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({ "message": "Wrong body fields", "documentation_url": "docs/api/v1" }, null, 2));
                    })
            } else {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ "message": "pic1 or pic2 is undefined", "documentation_url": "docs/api/v1" }, null, 2));
            }
        }
        basicAuth(addCar, null, req, res);
    });

// app.put('/api/v1/cars/update/:id(\\d+)',
//     (req, res) => {
//         let id = req.params.id;
//         storage.getById(id)
//             .then(car => {
//                 let model = req.body.model === undefined ? car.model : req.body.model;
//                 let size_class = req.body.size_class === undefined? car.size_class : req.body.size_class;
//                 let doors = req.body.doors === undefined ? car.doors : req.body.doors;
//                 let engine_volume = req.body.engine_volume === undefined ? car.engine_volume : req.body.engine_volume;
//                 let engine_type = req.body.engine_type === undefined ? car.engine_type : req.body.engine_type;
//                 let start_date = req.body.start_date === undefined ? car.start_date : req.body.start_date;
//                 let pic1 = req.files.pic1 === undefined ? car.pic1 : req.files.pic1;
//                 let pic2 = req.files.pic2 === undefined ? car.pic2 : req.files.pic2;
//                 storage.update({
//                     _id: id,
//                     model,
//                     size_class,
//                     doors,
//                     engine_volume,
//                     engine_type,
//                     start_date,
//                     pic1,
//                     pic2
//                 });
//                 console.log({
//                     _id: id,
//                     model,
//                     size_class,
//                     doors,
//                     engine_volume,
//                     engine_type,
//                     start_date,
//                     pic1: car.pic1,
//                     pic2: car.pic2,
//                 });
//                 res.setHeader('Content-Type', 'application/json');
//                 res.send(JSON.stringify({ "message": "Object updated", "documentation_url": "docs/api/v1" }, null, 2));
//         })
//     });

router.put('/api/v1/cars/update/:id(\\d+)',
    (req, res) => {
        function updateCar() {
            let id = req.params.id;
            console.log("zdarova");
            storage.getById(id)
                .then(car => {
                    console.log("zdarova1");
                    let model = req.body.model === undefined ? car.model : req.body.model;
                    let size_class = req.body.size_class === undefined ? car.size_class : req.body.size_class;
                    let doors = req.body.doors === undefined ? car.doors : req.body.doors;
                    let engine_volume = req.body.engine_volume === undefined ? car.engine_volume : req.body.engine_volume;
                    let engine_type = req.body.engine_type === undefined ? car.engine_type : req.body.engine_type;
                    let start_date = req.body.start_date === undefined ? car.start_date : req.body.start_date;
                    console.log(req.files);
                    let pic1 = req.files.pic1.name === undefined ? car.pic1 : req.files.pic1.name;
                    let pic2 = req.files.pic2.name === undefined ? car.pic2 : req.files.pic2.name;
                    // let pic1 = car.pic1;
                    // let pic2 = car.pic2;
                    console.log("zdarova2");
                    storage.update({
                        _id: id,
                        model,
                        size_class,
                        doors,
                        engine_volume,
                        engine_type,
                        start_date,
                        pic1,
                        pic2
                    })
                        .then(() => {
                            console.log("zdarova3");
                            if (req.files.pic1.name.match(/\.(jpeg|jpg|gif|png)$/) != null && req.files.pic1.name.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                                let format1 = fname1.split('.').pop();
                                let format2 = fname2.split('.').pop();
                                storage.updateImage({ data: req.files.pic1.data, imageName: req.files.pic1.name, contentType: format1 })
                                    .then(() => {
                                        storage.updateImage({ data: req.files.pic2.data, imageName: req.files.pic2.name, contentType: format2 });
                                        console.log("zdarova4");
                                    })
                                    .catch(err => { console.error(err) })
                            } else {
                                console.error("Only images are allowed!");
                            }
                        })
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": "Object updated", "documentation_url": "docs/api/v1" }, null, 2));
                })
        }

        basicAuth(updateCar, req.params.id, req, res);
    });


router.get('/api/v1/car/:id(\\d+)',
    (req, res) => {
        function getCar(param) {
            storage.getById(param)
                .then(car => {
                    let outCar = {
                        "id": car._id,
                        "model": car.model,
                        "size_class": car.size_class,
                        "doors": car.doors,
                        "engine_volume": car.engine_volume,
                        "engine_type": car.engine_type,
                        "start_date": car.start_date,
                        "pic1": car.pic1,
                        "pic2": car.pic2,
                        "api_url": "/api/v1/car/" + car._id
                    };
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outCar, null, 2));
                })
                .catch(error => {
                    console.log(error);
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": "404 not found", "documentation_url": "docs/api/v1" }, null, 2));
                });
        }

        basicAuth(getCar, req.params.id, req, res);
    });

router.get('/api/v1/user/:username',
    (req, res) => {
        function getUser(param) {
            storage.userGetByUsername(param)
                .then(user => {
                    let outUser = {
                        "id": user._id,
                        "username": user.username,
                        "role": user.role,
                        "api_url": "/api/v1/user/" + user.username
                    };
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(outUser, null, 2));
                })
                .catch(error => {
                    console.log(error);
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify({ "message": "404 not found", "documentation_url": "docs/api/v1" }, null, 2));
                });
        }
        basicAuth(getUser, req.params.username, req, res, "admin");
    });


module.exports = router;