const cars = require('./cars.js');

function askQuestion() {
    process.stdout.write('Input command, separator __ : ');
}

function processInput(buffer) {
    let inputString = buffer.toString().trim();
    console.log(`You've entered: '${inputString}'`);
    if (inputString) {
        // @todo process input
        // ...
        inputString = inputString.trim();
        if(IsValidInput(inputString))
            whichCommand(inputString);
        else 
            console.log('Sore, you ve just inputed invalid command');
        askQuestion();  // ask the question again
    } else {
        console.log(`Exit.`);
        process.stdin.end();  // stop listening input
    }
}

function whichCommand(inputString) {
    let result = inputString.split('__');
    switch(result[0]) {
        case 'create':
            cars.create({
                "id": cars.GenerateId(),
                "model": result[1],
                "size_class": result[2],
                "doors": parseInt(result[3]),
                "engine_volume": parseInt(result[4]),
                "start_date": result[5]
            })
            .catch(error => console.log(error));
        break;

        case 'getall':
            cars.getAll()
                .then(data => console.log(data))
                .catch(error => console.log(error));
        break;

        case 'getbyid':
            cars.getById(parseInt(result[1]))
                .then(data => console.log(data))
                .catch(error => console.log(error));
        break;

        case 'update':
            cars.update({
                "id": parseInt(result[1]),
                "model": result[2],
                "size_class": result[3],
                "doors": parseInt(result[4]),
                "engine_volume": parseInt(result[5]),
                "start_date": result[6]
            })
            .catch(error => console.log(error));
        break;

        case 'remove':
            cars.remove(parseInt(result[1]))
                .catch(error => console.log(error));
        break;  
    }
}

function IsValidInput(inputStr) {
    const regexArr = [
        /create__([0-9A-Za-z ]+)__([0-9A-Za-z +]+)__\d__\d+__\d{2}[./-]\d{2}[./-]\d{4}/,
        /getall/,
        /getbyid__\d+/,
        /update__\d+___([0-9A-Za-z ]+)__([0-9A-Za-z +]+)__\d__\d+__\d{2}[./-]\d{2}[./-]\d{4}/,
        /remove__\d+/,
    ];

    for(let i of regexArr)
        if(i.test(inputStr)) return true;
    return false;
}

module.exports = {
    askQuestion,
    processInput
};