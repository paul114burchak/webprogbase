const express = require('express');
const bodyParser = require('body-parser');
const fileload = require('express-fileupload');
let routeAdd = require('./routes/add');
let routeAutos = require('./routes/autos');
let routeAuto = require('./routes/auto');
let routes = require('./routes/index');
let routerReg = require('./routes/register');
let rourtAuth = require('./routes/auth');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileload());
app.use(cookieParser());
app.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/',routes);
app.use('/',routerReg);
app.use('/',rourtAuth);
app.use('/',routeAdd);
app.use('/',routeAutos);
app.use('/',routeAuto);

app.listen(process.env.PORT || 5000);