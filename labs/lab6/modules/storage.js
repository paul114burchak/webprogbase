const mongoose = require('mongoose');

const url = "mongodb://localhost:27017/db";

mongoose.connect(url);
let Schema = mongoose.Schema;

let schema = new Schema({
    _id: Number,
    model: String,
    size_class: String,
    doors: Number,
    engine_volume: Number,
    engine_type: String,
    start_date: String,
    pic1: { data: Buffer, contentType: String },
    pic2: { data: Buffer, contentType: String }
});

let car = mongoose.model('car', schema);

function create(x) {
    let newCar = new car(x);
    const promise = newCar.save()
        .then(y => console.log("Created: %s", y))
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
};

function getAll() {
    const promise = car.find()
        .then(cars => {
            return cars;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
};

function getById(x_id) {
    const promise = car.findOne({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function update(x) {
    let updates = new car(x);
    const promise = car.findOneAndUpdate(
        { _id: x._id },
        { $set: updates })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function remove(x_id) {
    const promise = car.remove({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
}

let SchemaUser = new Schema({
    _id: Number,
    username: String,
    password: String,
    role: String,
});

let user = mongoose.model('user', SchemaUser);

function userCreate(x) {
    let newUser = new user(x);
    const promise = newUser.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function userGetById(x_id) {
    const promise = user.findOne({ _id: x_id })
        .then(user => { console.log('user getted id'); return user; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function userGetByUsername(x_username) {
    const promise = user.findOne({ username: x_username })
        .then(user => { console.log('user getted x_username'); return user; })
        .catch(error => { throw error + x_username; });
    return promise;
}

function userGet(x_username, x_password) {
    const promise = user.findOne({ username: x_username, password: x_password })
        .then(user => {
            console.log("user by password");
            return user;
        })
        .catch(error => { throw error + x_username; });
    return promise;
}

function usersGetAll() {
    const promise = user.find()
        .then(users => {
            return users;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
}

module.exports = {
    getAll,
    getById,
    update,
    remove,
    create,
    userCreate,
    userGet,
    userGetById,
    usersGetAll,
    userGetByUsername
};