const storage = require('../modules/storage.js');
const auth = require('../modules/auth.js');

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
let router = express.Router();

router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

passport.use(new LocalStrategy(
    function (username, password, done) {
        let hash = sha512(password, serverSalt).passwordHash;
        console.log(username, hash);
        storage.userGet(username, hash)
            .then(user => {
                console.log(user);
                done(user ? null : 'Invalid username or password', user);
            });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (_id, done) {
    storage.userGetById(_id)
        .then(user => {
            done(user ? null : 'No user', user);
        });
});

router.get('/login',
    (req, res) => {
        let message = "";
        res.render('login', { user: req.user, message });
    });

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
    }));

// router.post('/login', function (req, res, next) {
//     passport.authenticate('local', function (err, user, info) {
//         if (err || !user) {
//             let message = "Invalid username or password";
//             res.render('login', { user: req.user, message });
//         } else {
//             res.render('index', { user: user })
//         }
//     })(req, res, next);
// });

router.get('/logout/:id(\\d+)',
    auth.checkAuth,
    (req, res) => {
        req.logout();
        res.redirect('/');
    });

router.get('/admin/:id(\\d+)',
    auth.checkAdmin,
    (req, res) => {
        storage.usersGetAll()
            .then(users => res.render('admin', { users, user: req.user }))
            .catch(error => {
                console.log(error);
                res.sendStatus(404);
            });
    });

router.get('/profile/:id(\\d+)',
    auth.checkAuth,
    (req, res) => res.render('profile', { user: req.user }));

module.exports = router;