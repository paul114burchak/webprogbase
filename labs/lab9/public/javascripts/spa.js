window.onload = function () {
var app = new Vue({
    el: '#app',
    data: {
      cars:[],
      searchString:"",
      idToRemove:0,
      totalPages: 0,
      currentPage: 1,
    },
    methods: {
        getCars: function () {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              let cars = JSON.parse(xhttp.response);
              app.cars = [];
              for(let i = 0; i< cars.length;i++) 
              if(cars[i].model.includes(app.searchString))
              app.cars.push(cars[i]);
              console.log(app.cars);
            }
          };
          xhttp.open("GET", "/api/v1/cars", true);
          xhttp.send();
        },
        removeCar: function () {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            console.log(xhttp.readyState, xhttp.response, xhttp.status);
            if (this.readyState == 4 && this.status == 200) {
              app.getCars();
            }
          };
          console.log(app.idToRemove);
          xhttp.open("DELETE","/api/v1/cars/delete/"+ app.idToRemove,true);
          xhttp.send();
        },
        addCar: function () {
          let data = $('#createForm')[0];
          let file = new FormData(data);
          console.log(data);
          $.ajax({
            url: "/api/v1/cars/add",
            type: 'POST',
            data: file,
            success: function (data) {
              console.log(data);					
              app.getCars();
            },
            error: function (e) {
              console.log(e);
            },
            cache: false,
            contentType: false,
            processData: false
          });
        },
        search: function () {
          
        },
        remove: function(id) {
          app.idToRemove = id;
          console.log(app.idToRemove,id);
        }
      },
      beforeMount(){
        this.getCars()
      },
      watch:{
        searchString: function(data) {
          app.searchString = data;
          app.getCars();
        }
      }
  });
}

Vue.component('car', {
    // Компонент todo-item теперь принимает
    // "prop", то есть пользовательский параметр.
    // Этот параметр называется todo.
    props: ['model','href','pic2','id','remove'],
    template: '<div class="carCss">\
                <a v-bind:href="href">\
                        <div class="scale" v-bind:style="pic2">\
                                <span class="mylabel">{{model}}</span>\
                        </div>\
                </a>\
                <button class="buttonRemove" href="#" v-bind:id="id" data-toggle="modal" data-target="#myModal" v-on:click="remove(id)"><span class="glyphicon glyphicon-remove"></span></button>\
              </div>'
  });