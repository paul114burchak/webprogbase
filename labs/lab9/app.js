const express = require('express');
const bodyParser = require('body-parser');
const fileload = require('express-fileupload');
let routeAdd = require('./routes/add');
let routeAutos = require('./routes/autos');
let routeAuto = require('./routes/auto');
let routes = require('./routes/index');
let routerReg = require('./routes/register');
let rourtAuth = require('./routes/auth');
let api = require('./routes/api');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const storage = require('./modules/storage.js');
let auth = require('basic-auth');

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileload());
app.use(cookieParser());
app.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.put('/api/v1/cars/update/:id(\\d+)',
(req, res) => {
    function updateCar() {
        let id = req.params.id;
        console.log("zdarova");
        storage.getById(id)
            .then(car => {
                console.log("zdarova1");
                let model = req.body.model === undefined ? car.model : req.body.model;
                let size_class = req.body.size_class === undefined ? car.size_class : req.body.size_class;
                let doors = req.body.doors === undefined ? car.doors : req.body.doors;
                let engine_volume = req.body.engine_volume === undefined ? car.engine_volume : req.body.engine_volume;
                let engine_type = req.body.engine_type === undefined ? car.engine_type : req.body.engine_type;
                let start_date = req.body.start_date === undefined ? car.start_date : req.body.start_date;
                console.log(req.files);
                let pic1 = req.files.pic1.name === undefined ? car.pic1 : req.files.pic1.name;
                let pic2 = req.files.pic2.name === undefined ? car.pic2 : req.files.pic2.name;
                // let pic1 = car.pic1;
                // let pic2 = car.pic2;
                console.log("zdarova2");
                storage.update({
                    _id: id,
                    model,
                    size_class,
                    doors,
                    engine_volume,
                    engine_type,
                    start_date,
                    pic1,
                    pic2
                })
                    .then(() => {
                        console.log("zdarova3");
                        if (req.files.pic1.name.match(/\.(jpeg|jpg|gif|png)$/) != null && req.files.pic1.name.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                            let format1 = fname1.split('.').pop();
                            let format2 = fname2.split('.').pop();
                            storage.updateImage({ data: req.files.pic1.data, imageName: req.files.pic1.name, contentType: format1 })
                                .then(() => {
                                    storage.updateImage({ data: req.files.pic2.data, imageName: req.files.pic2.name, contentType: format2 });
                                    console.log("zdarova4");
                                })
                                .catch(err => { console.error(err) })
                        } else {
                            console.error("Only images are allowed!");
                        }
                    })
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ "message": "Object updated", "documentation_url": "docs/api/v1" }, null, 2));
            })
    }

    basicAuth(updateCar, req.params.id, req, res);
});

app.post('/api/v1/cars/add',
    (req, res) => {
        function addCar() {
            if (req.files.pic1 != undefined || req.files.pic2 != undefined) {
                let fname1 = req.files.pic1.name;
                let fname2 = req.files.pic2.name;
                storage.create({
                    "_id": Math.round(Math.random() * (10000 - 1000) + 1000),
                    "model": req.body.model,
                    "size_class": req.body.size_class,
                    "doors": parseInt(req.body.doors),
                    "engine_volume": parseInt(req.body.engine_volume),
                    "engine_type": req.body.engine_type,
                    "start_date": req.body.start_date,
                    "pic1": fname1,
                    "pic2": fname2
                })
                    .then(() => {
                        if (fname1.match(/\.(jpeg|jpg|gif|png)$/) != null && fname2.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                            let format1 = fname1.split('.').pop();
                            let format2 = fname2.split('.').pop();
                            storage.updloadImage(req.files.pic1.data, fname1, format1, req.files.pic2.data, fname2, format2)
                                .catch(err => { console.error(err) })
                        } else {
                            console.error("Only images are allowed!");
                        }
                    })
                    .then(() => {
                        console.log("after img");
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({ "message": "Object created", "documentation_url": "docs/api/v1" }, null, 2));
                    })
                    .catch(error => {
                        console.log(error);
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({ "message": "Wrong body fields", "documentation_url": "docs/api/v1" }, null, 2));
                    })
            } else {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ "message": "pic1 or pic2 is undefined", "documentation_url": "docs/api/v1" }, null, 2));
            }
        }
        basicAuth(addCar, null, req, res);
    });

const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

function basicAuth(callback, param, req, res) {
    let credentials = auth(req);
    if (credentials) {
        console.log(credentials.name, credentials.pass);
        storage.userGet(credentials.name, sha512(credentials.pass, serverSalt).passwordHash)
            .then(() => {
                callback(param);
            })
            .catch(() => {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ "message": "Invalid username or password", "documentation_url": "docs/api/v1" }, null, 2));
            });
    } else {
        res.set('WWW-Authenticate', 'Basic realm="example"');
        //res.send(JSON.stringify({ "message": "Required auth", "documentation_url": "docs/api/v1" }, null, 2));
        res.send(401);
    }
}
app.use('/', routes);
app.use('/register', routerReg);
app.use('/', rourtAuth);
app.use('/add', routeAdd);
app.use('/autos', routeAutos);
app.use('/auto', routeAuto);
app.use('/', api);

app.get("/docs/api/v1",
    (req, res) => {
        res.render('api', { user: req.user })
    });

app.get('/image/:imgnm', (req, res) => {
    storage.getImage(req.params.imgnm)
        .then(data => {
            res.setHeader('Cache-Control', 'public, max-age=3000000');
            res.contentType(data.contentType);
            res.send(data.data);
        })
        .catch(err => { console.error(err) })
});


app.use(function (req, res, next) {
    res.status(404);
    res.render('404', { user: req.user });
    return;
});

app.listen(process.env.PORT || 5000);