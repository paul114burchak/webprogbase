function checkAuth(req, res, next) {
    if (!req.user)  return res.render('notauth', {
        user: req.user
    }) // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

function checkAdmin(req, res, next) {
    if (!req.user) res.render('notauth', {
        user: req.user
    }) // 'Not authorized'
    else if (req.user.role !== 'admin') res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

module.exports = {
    checkAuth,
    checkAdmin
};