const storage = require('../modules/storage.js');
const auth = require('../modules/auth.js');
const express = require('express');
const bodyParser = require('body-parser');
let router = express.Router();

router.get('/',
    auth.checkAuth,
    (req, res) => res.render('add', {user: req.user}));

router.post('/auto',
        auth.checkAuth,
        (req, res) => {
            if (req.files.pic1 != undefined || req.files.pic2 != undefined) {
            let fname1 = req.files.pic1.name;
            let fname2 = req.files.pic2.name;
        storage.create({
            "_id": Math.round(Math.random() * (10000 - 1000) + 1000),
            "model": req.body.model,
            "size_class": req.body.size,
            "doors": parseInt(req.body.doors),
            "engine_volume": parseInt(req.body.vol),
            "engine_type": req.body.engine,
            "start_date": req.body.date,
            "pic1": fname1,
            "pic2": fname2
        })
            .then(() => {
                if (fname1.match(/\.(jpeg|jpg|gif|png)$/) != null && fname2.match(/\.(jpeg|jpg|gif|png)$/) != null) {
                    let format1 = fname1.split('.').pop();
                    let format2 = fname2.split('.').pop();
                    storage.updloadImage(req.files.pic1.data, fname1, format1,req.files.pic2.data, fname2, format2)
                        .catch(err => { console.error(err) })
                } else {
                    console.error("Only images are allowed!");
                }
            });
        res.redirect('/');
        } else res.send("Error: You didnt select files to upload");
    });

module.exports = router;