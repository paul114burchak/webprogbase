const auto = require('./modules/cars.js')
const express = require('express');

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', 
    (req,res) => res.render('index'));

app.get('/autos',
    (req,res) => {
        auto.getAll()
            .then(cars => {
                res.render('autos', {
                    cars
                });
            })
            .catch(err => {
                console.log(err);
            });
    
    });

app.get('/auto/:auto_id(\\d+)',
   (req, res) => {
        auto.getById(parseInt(req.params.auto_id))
            .then(auto => {
                res.render('auto', {
                    auto
                });
            })
            .catch(err => {
                console.log(err);
                res.sendFile(__dirname + '/views/404.html');
            });
    });

app.listen(3002, () => console.log("UP!"));