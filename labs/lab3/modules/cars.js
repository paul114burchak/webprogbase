const fs = require('fs-promise');

function create(x) {
    const promise = getAll()
        .then(cars => {
            for(let i of cars) 
                while (i.id === x.id) 
                    x.id = GenerateId();
            cars.push(x);
            const jsonStr = JSON.stringify({"cars": cars});
            return jsonStr;
        })
        .then(jsonStr => fs.writeFile('cars.json',jsonStr))
        .catch(error => { 
            throw error;
        });
    return promise;
};

function getAll() {
    let cars = [];
    const promise = fs.readFile('cars.json')
        .then(fileContent => {
            const jsonData = JSON.parse(String(fileContent));
            for (let i of jsonData.cars) {
                cars.push(i);
            }
            return cars;
        })
        .catch((error) => {
             throw 'Get all error: ' + error;
            });
    return promise;
};

function getById(x_id) {
    const promise = getAll()
        .then(cars => {
            for (let i of cars) {
                if(i.id === x_id)
                return i;
            }
            throw 'there is no user with id: ';
        })
        .catch(error => { throw error + x_id;});
    return promise;
};

function GenerateId() {
    return Math.round(Math.random() * (10000 - 1000) + 1000);
}

function update(x) {
    const promise = getAll()
        .then(cars => {
            for (let i in cars) {
                if(cars[i].id === x.id) {
                    cars.splice(i, 1,x);
                    const jsonStr = JSON.stringify({"cars": cars});
                    return jsonStr;
                }
            }
            throw 'invalid id';
        })
        .then(jsonStr => fs.writeFile('cars.json',jsonStr))
        .catch(error => { 
            throw error;
        });
    return promise;
};

function remove(x_id) {
    const promise = getAll()
        .then(cars => {
            for (let i in cars) {
                if(cars[i].id === x_id) {
                    cars.splice(i,1);
                    const jsonStr = JSON.stringify({"cars": cars});
                    return jsonStr;
                }
            }
            throw 'invalid id';
        })
        .then(jsonStr => fs.writeFile('cars.json',jsonStr))
        .catch(error => { 
            throw error;
        });
    return promise;
}

module.exports = {
    getAll,
    getById,
    update,
    remove,
    create,
    GenerateId
};
