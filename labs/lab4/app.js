const auto = require('./modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
let routeAdd = require('./routes/add');
let routeAutos = require('./routes/autos');
let routeAuto = require('./routes/auto');
let routes = require('./routes/index');
const fileload = require('express-fileupload');

const app = express();
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileload());
app.use(busboyBodyParser());

app.use('/',routes);
app.use('/',routeAdd);
app.use('/',routeAutos);
app.use('/',routeAuto);

app.listen(3002, () => console.log("UP!"));