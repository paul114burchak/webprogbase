const auto = require('../modules/cars.js')
const express = require('express');
const busboyBodyParser = require('busboy-body-parser');
const bodyParser = require('body-parser');
let router = express.Router();

router.get('/autos/',
    (req, res) => {
        const page = parseInt(req.query.page);
        auto.getAll()
            .then(cars => {
                let search = "$";
                res.render('autos', {
                    cars, page, search
                });
            })
            .catch(err => {
                console.log(err);
            });
    });

router.get('/autos/search/',
    (req, res) => {
        const page = parseInt(req.query.page);
        const search = req.query.search;
        auto.getAll()
            .then(cars => {
                let carsFilter = [];
                for (let i in cars) {
                    if (cars[i].model.includes(search))
                        carsFilter.push(cars[i]);
                }
                cars = carsFilter.slice();
                res.render('autos', {
                    cars, page, search
                });
            })
            .catch(err => {
                console.log(err);
            });
    });

module.exports = router;
