const auto = require('../modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const fs = require('fs');
let router = express.Router();

router.get('/auto/:auto_id(\\d+)',
    (req, res, next) => {
        auto.getById(parseInt(req.params.auto_id))
            .then(auto => {
                res.render('auto', {
                    auto
                });
            })
            .catch(err => {
                console.log(err);
            });
    });

router.post('/auto/delete/:auto_id(\\d+)',
    (req, res) => {
        const id = parseInt(req.params.auto_id);
        auto.getById(id)
            .then(car => {
                fs.unlinkSync('/home/firefly/wp/webprogbase/labs//lab4/public' + car.pic_path);
                fs.unlinkSync('/home/firefly/wp/webprogbase/labs//lab4/public' + car.pic_path2);
            })
            .catch(err => {
                console.log(err);
            });
        auto.remove(id);
        res.redirect('/autos?page=1')
    });

module.exports = router;
