const auto = require('../modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const fs = require('fs');
let router = express.Router();

router.get('/add',
    (req, res) => res.render('add'));

router.post('/add/auto',
    (req, res) => {
        if (req.files.pic1 != undefined || req.files.pic2 != undefined) {
            fs.writeFileSync('/home/firefly/wp/webprogbase/labs//lab4/public/images/' + req.files.pic1.name, req.files.pic1.data);
            fs.writeFileSync('/home/firefly/wp/webprogbase/labs//lab4/public/images/' + req.files.pic2.name, req.files.pic2.data);
            auto.create({
                "id": Math.round(Math.random() * (10000 - 1000) + 1000),
                "model": req.body.model,
                "size_class": req.body.size,
                "doors": parseInt(req.body.doors),
                "engine_volume": parseInt(req.body.vol),
                "engine_type": req.body.engine,
                "start_date": req.body.date,
                "pic_path": '/images/' + req.files.pic1.name,
                "pic_path2": '/images/' + req.files.pic2.name
            })
            res.redirect('/');
        } else res.send("Error: You didnt select any files to upload");
    });

module.exports = router;