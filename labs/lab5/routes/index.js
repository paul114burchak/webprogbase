const auto = require('../modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const fs = require('fs');
var router = express.Router();


router.get('/',
    (req, res) => res.render('index'));

module.exports = router;
