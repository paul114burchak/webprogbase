const auto = require('../modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
let router = express.Router();

router.get('/add',
    (req, res) => res.render('add'));

router.post('/add/auto',
    (req, res) => {
        if (req.files.pic1 != undefined || req.files.pic2 != undefined) {
            let fname1 = req.files.pic1.name;
            let fname2 = req.files.pic2.name;
            
            let type1 = "image/"+ fname1.slice(fname1.indexOf('.')+1);
            let type2 = "image/"+ fname2.slice(fname2.indexOf('.')+1);
        auto.create({
            "_id": Math.round(Math.random() * (10000 - 1000) + 1000),
            "model": req.body.model,
            "size_class": req.body.size,
            "doors": parseInt(req.body.doors),
            "engine_volume": parseInt(req.body.vol),
            "engine_type": req.body.engine,
            "start_date": req.body.date,
            "pic1": {data: req.files.pic1.data, contentType: type1},
            "pic2": {data: req.files.pic2.data, contentType: type2}
        });
        res.redirect('/');
        } else res.send("Error: You didnt select any files to upload");
    });

module.exports = router;