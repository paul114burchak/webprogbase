const auto = require('../modules/cars.js')
const express = require('express');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const fs = require('fs');
var router = express.Router();

router.get('/auto/:auto_id(\\d+)',
    (req, res, next) => {
        auto.getById(parseInt(req.params.auto_id))
            .then(auto => {
                res.render('auto', {
                    auto
                });
            })
            .catch(err => {
                console.log(err);
            });
    });

router.post('/auto/delete/:auto_id(\\d+)',
    (req, res) => {
        const id = parseInt(req.params.auto_id);
        auto.remove(id);
        res.redirect('/autos?page=1')
    });

module.exports = router;
