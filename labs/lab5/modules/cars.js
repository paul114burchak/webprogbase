const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds241065.mlab.com:41065/heroku_mdtkfbsp";

mongoose.connect(url);
let Schema = mongoose.Schema;

let schema = new Schema({
    _id: Number,
    model: String,
    size_class: String,
    doors: Number,
    engine_volume: Number,
    engine_type: String,
    start_date: String,
    pic1: { data: Buffer, contentType: String },
    pic2: { data: Buffer, contentType: String }
});

let car = mongoose.model('car', schema);

function create(x) {
    let newCar = new car(x);
    const promise = newCar.save()
        .then(y => console.log("Created: %s", y))
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
};

function getAll() {
    const promise = car.find()
        .then(cars => {
            return cars;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
};

function getById(x_id) {
    const promise = car.findOne({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function update(x) {
    let updates = new car(x);
    const promise = car.findOneAndUpdate(
        { _id: x._id },
        { $set: updates })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
};

function remove(x_id) {
    const promise = car.remove({ _id: x_id })
        .then(car => { return car })
        .catch(error => { throw error + x_id; });
    return promise;
}

module.exports = {
    getAll,
    getById,
    update,
    remove,
    create
};