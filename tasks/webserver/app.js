'use strict';
let express = require('express');

let app = express();

const arr = [4,2,42,56,64,256,35];

app.get('/', (req, res) => {
    res.json({arr: arr});
});

app.get("/at/:index",(req, res) => {
    const ind = req.params.index; 
    if (ind < arr.length) {
        res.json({
            value: arr[ind]
        });
        res.send();
    } else {
        res.sendStatus(404);
    }
});

app.post("/:value", (req, res) => {
    const val = req.params.value; 
    arr.push(val);
    res.send("added");
})
app.listen(3000, () => console.log("start"));