const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
let app = express();

app.set('view engine', 'ejs');

app.get('/',
    (req, res) => res.render('index'));

app.get('/login',
    (req, res) => res.render('login'));

app.get('/profile',
    (req, res) => res.render('profile'));

app.get('/admin',
    (req, res) => res.render('admin'));

app.listen(3000, () => console.log('up!'));
